# Config drive preparation

## Prepare model folder
```
export MCP_VERSION="2018.4.0"
export version="tags/2018.4.0"
apt-get update && apt-get install mkisofs git -y
```
```
mkdir /root/model
cd /root/
git clone https://rmelero:946PA9MVfh2GZe9Gxkcj@bitbucket.org/mirantis-training/mcp200-reclass-model model
git clone https://github.com/Mirantis/reclass-system-salt-model/ /root/model/classes/system
pushd /root/model/classes/system
git checkout ${MCP_VERSION} ; popd
```
## Download scripts and repos
```
wget -O /root/create-config-drive https://raw.githubusercontent.com/Mirantis/mcp-common-scripts/${MCP_VERSION}/config-drive/create_config_drive.sh
chmod +x /root/create-config-drive
wget -O /root/user_data.sh https://raw.githubusercontent.com/Mirantis/mcp-common-scripts/${MCP_VERSION}/config-drive/master_config.sh
git clone https://github.com/Mirantis/mk-pipelines.git /root/mk-pipelines
git clone https://github.com/Mirantis/pipeline-library.git /root/pipeline-library
pushd /root/mk-pipelines
git checkout ${version} ; popd
pushd /root/pipeline-library
git checkout ${version} ; popd
```

## Edit /root/user_data.sh to have the correct IPs for the deploy network

```
sed -i 's/172.16.164.15/192.168.1.15/' /root/user_data.sh
sed -i 's/172.16.164.1/192.168.1.1/' /root/user_data.sh
sed -i 's/255.255.255.192/255.255.255.0/' /root/user_data.sh
sed -i 's/cfg01.deploy-name.local/cfg01.trainings.local/' /root/user_data.sh
sed -i 's/cp \-r \/mnt\/mk-pipelines\/\* \/home\/repo\/mk\/mk-pipelines\//cp \-a \/mnt\/mk-pipelines\/. \/home\/repo\/mk\/mk-pipelines\//' /root/user_data.sh

sed -i 's/cp \-r \/mnt\/pipeline-library\/\* \/home\/repo\/mcp-ci\/pipeline-library\//cp \-a \/mnt\/pipeline-library\/. \/home\/repo\/mcp-ci\/pipeline-library\//' /root/user_data.sh
```

## Create config drive
```
/root/create-config-drive -u /root/user_data.sh -h cfg01 --model /root/model --mk-pipelines /root/mk-pipelines --pipeline-library /root/pipeline-library cfg01-config.iso
```

# Lab instructions

# TEMP DNS FIX

```
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
```

## Create bridges
```
cat << 'EOF' > br_net_mgm.xml
<network>
 <name>br_mgm</name>
 <bridge name="br_mgm"/>
 <forward mode="nat"/>
 <ip address="192.168.1.1" netmask="255.255.255.0" />
</network>
EOF

cat << 'EOF' > br_net_ctl.xml
<network>
 <name>br_ctl</name>
 <bridge name="br_ctl"/>
 <forward mode="nat"/>
 <ip address="192.168.2.1" netmask="255.255.255.0" />
 <ip address="192.168.2.241" netmask="255.255.255.0" />
</network>
EOF

cat << 'EOF' > br_net_ext.xml
<network>
 <name>br_ext</name>
 <bridge name="br_ext"/>
 <forward mode="nat"/>
 <ip address="192.168.3.1" netmask="255.255.255.0" />
</network>
EOF

for net in mgm ctl ext
do
virsh net-define br_net_${net}.xml
virsh net-autostart br_${net}
virsh net-start br_${net}
done
```

## Create cfg01 libvirt dir

```
mkdir -p /pool/images/cfg01/
```

## Set your version variable

```
export MCP_VERSION="2018.4.0"
```

## Download day01 cfg MCP image and place in libvirt folder

```
wget http://192.168.121.40/cfg01-day01-2018.4.0.qcow2 -O /pool/images/cfg01/system.qcow2 -q
```

## Copy config drive and place in libvirt folder (TODO: best practice for generatating config drive)
```
wget http://192.168.121.40/cfg01-config.iso -O /pool/images/cfg01/cfg01-config.iso
```

## Download define vm script and make it executable

```
wget https://raw.githubusercontent.com/Mirantis/mcp-common-scripts/${MCP_VERSION}/predefine-vm/define-vm.sh -O /root/define-vm.sh
chmod 0755 /root/define-vm.sh
```

## Setup variables for deploying cfg host

```
export VM_NAME="cfg01.trainings.local"
export VM_SOURCE_DISK="/pool/images/cfg01/system.qcow2"
export VM_CONFIG_DISK="/pool/images/cfg01/cfg01-config.iso"
export VM_MGM_BRIDGE_NAME="br_mgm"
export VM_CTL_BRIDGE_NAME="br_ctl"
```

## Run define-vm script

```
/root/define-vm.sh
```

## Start cfg01 VM and connect to console

```
virsh start ${VM_NAME}; virsh console ${VM_NAME}
```

## Wait for cloud-init to finish

```
[   97.220698] cloud-init[1373]: Cloud-init v. 17.1 finished at Wed, 13 Jun 2018 20:40:10 +0000. Datasource DataSourceNoCloud [seed=/dev/sr0][dsmode=net].  Up 97.20 seconds
```

## Enable DHCP server

```
maas-region apikey --username=mirantis > /tmp/key
maas login mirantis http://localhost:5240/MAAS/api/2.0 - < /tmp/key
maas mirantis vlan update fabric-0 untagged dhcp_on=True primary_rack=cfg01
```

## Temp workaround for hardcoded images directory until https://gerrit.mcp.mirantis.net/#/c/23550/ is merged

```
sed -i 's/var\/lib\/libvirt/pool/' /srv/salt/env/prd/salt/meta/salt.yml
```

## workaround for bad cinder volume type creation

```
cd /srv/salt/env/prd
echo "288c288
< {%- if 'version' in controller and controller.version in ['mitaka', 'newton'] %}
---
> {%- if 'version' in controller and controller.version in ['mitaka', 'newton', 'ocata'] %}" > volume_type_fix
patch cinder/controller.sls -i volume_type_fix
```
## Connect salt-minion on the foundation node

### Install salt-minion version 2016.3

```
echo 'deb [arch=amd64] http://mirror.mirantis.com/2018.4.0/saltstack-2016.3/xenial/ xenial main' > /etc/apt/sources.list.d/saltstack.list
apt-key adv --keyserver keyserver.ubuntu.com --recv 0E08A149DE57BFBE
apt-get update && apt-get install salt-minion -y
```

### Add configuration to connect to salt-master

```
echo "master: 192.168.1.15" >> /etc/salt/minion
echo "id: kvm01.trainings.local" >> /etc/salt/minion
```
### Restart salt-minion service
```
systemctl restart salt-minion.service
```
### Check for accepted salt-minion on master
```
root@cfg01:~# salt-key
Accepted Keys:
cfg01.trainings.local
kvm01.trainings.local
Denied Keys:
Unaccepted Keys:
Rejected Keys:
```

### workaround for libvirt package downgrade
#### default package on image needs to be downgraded for fix

```
salt-call state.apply linux.system.repo
apt-get install libvirt-bin=1.3.1-1ubuntu10.1~xenial1+contrail1 -y --allow-downgrades
```

## Login to Jenkins

Navigate to [Jenkins](http://192.168.2.15:8081/) or http://192.168.2.15:8081/

```
Username: admin
Password: r00tme
```

After login, open the [Deploy - Openstack](http://192.168.2.15:8081/job/deploy_openstack/) build by pressing the "play" button on the right

Change two parameters to prepare physical hosts and deploy the cicd cluster:

| Parameter | Value
| --- | --- |
| STACK_INSTALL | core,kvm,cicd
| STATIC_MGMT_NETWORK | Checked |

Click on `Build`

Follow the build by clicking on triangle that appears by the build number in the `Build History` box on the left and selecting `Console Output`

  Once the build has completed, you should see
  ```
  [Pipeline] End of Pipeline
  Finished: SUCCESS
  ```

### Workaround for 'Cannot extend ID nova.db.offline_sync'

```
wget http://apt.mirantis.com.s3.amazonaws.com/xenial/pool/salt/s/salt-formula-nova/salt-formula-nova_2016.12.1%2B201808081443.9dd01c9-2xenial1_all.deb
dpkg -i salt-formula-nova_2016.12.1+201808081443.9dd01c9-2xenial1_all.deb
```

  You can now login to the main Jenkins. Navigate to [Jenkins](http://192.168.2.90:8081/) or http://192.168.2.90:8081/

  Get the password for Jenkins
  ```
  salt-call pillar.get _param:openldap_admin_password_generated
  ```
  ```
  local:
      xG9KHxdA9CcKYEeQ
  ```

  Navigate to the [Deploy - Openstack](http://192.168.2.90:8081/job/deploy_openstack/) build by pressing the "play" button on the right.

  Change two parameters to deploy OpenStack and Stacklight:

  | Parameter | Value
  | --- | --- |
  | STACK_INSTALL | ovs,openstack,stacklight,finalize
  | STATIC_MGMT_NETWORK | Checked |

Login to [Horizon](https://192.168.2.80/auth/login/?next=/) or https://192.168.2.80/auth/login/?next=/

```
salt-call pillar.get _param:keystone_admin_password_generated
```
```
local:
    b4evVVG1GsYmf2H4
```
Connect to controller
```
ssh stack@ctl01
password: stack
```
Load credentials into your environment
```
sudo -i
. keystonercv3
```
Create a VM from the CLI
```
openstack server create --image cirros --flavor small test-1 --nic net-id=internal
```
Allocate floating IP
```
openstack floating ip create --floating-ip-address 192.168.3.51 external
```
Attach floating IP to instance
```
openstack server add floating ip test-1 192.168.3.51
```
Connect to instance
```
ssh cirros@192.168.3.51
password: gocubsgo
```
Test instance networking
```
$ ping google.com
PING google.com (216.58.194.174): 56 data bytes
64 bytes from 216.58.194.174: seq=0 ttl=50 time=5.689 ms
64 bytes from 216.58.194.174: seq=1 ttl=50 time=7.746 ms
64 bytes from 216.58.194.174: seq=2 ttl=50 time=20.347 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 5.689/11.260/20.347 ms
```

# Add Compute Exercise

## Connect to cfg node
```
ssh stack@cfg01
password: stack
sudo -i
```
## Edit

# Addresses to checkout

| Tool | Address | Username | Password
| --- | --- |
| Horizon | https://192.168.2.80/auth/login/ | admin | r00tme
| Prometheus | http://192.168.2.80:15010/graph | N\A | N\A |
| Alertmanager | http://192.168.2.80:15011/#/alerts | N\A | N\A |
| Grafana | https://192.168.2.80:8084/login | admin | r00tme
| Kibana | https://192.168.2.80:5601/ | N\A | N\A |
| Jenkins | https://192.168.2.80:8081/login | admin | r00tme
| Gerrit | https://192.168.2.80:8070/login | admin | r00tme
| SaltStack Deployment Documentation | http://192.168.2.80:8090/index.html | N\A | N\A |
